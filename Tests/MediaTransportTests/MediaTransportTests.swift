    import XCTest
    @testable import MediaTransport

    final class MediaTransportTests: XCTestCase {
        func testExample() {
            // This is an example of a functional test case.
            // Use XCTAssert and related functions to verify your tests produce the correct
            // results.
            XCTAssertEqual(MediaTransport().text, "Hello, World!")
        }
    }
